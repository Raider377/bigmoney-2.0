﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpo.Logger;
using Ecng.Xaml;
using NPOI.SS.Formula.Functions;
using StockSharp.Algo;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Strategies.Protective;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Messages;
using StockSharp.Quik;
using StockSharp.Xaml;
using StockSharp.Xaml.Charting;

namespace StopLossTakeProfit
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private QuikTrader Trader { get; set; }
        private Strategy MyStrategy { get; set; }
        private Portfolio Portfolio { get; set; }
        private Security Security { get; set; }
        private TimeSpan Timeframe { get; set; }
        private CandleManager CandleManager { get; set; }
        private CandleSeries CandleSeries { get; set; }
        private ChartArea Area { get; set; }
        private ChartCandleElement ChartCandleElement { get; set; }
        private ChartOrderElement ChartOrderElement { get; set; }
        private ChartTradeElement ChartTradeElement { get; set; }
        private bool IsRealTime { get; set; }
        private bool _flag;
        private StockSharp.Logging.LogManager LogManager { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Trader = new QuikTrader();
            LogManager = new StockSharp.Logging.LogManager();
            LogManager.Listeners.Add(LogControl);
            LogManager.Sources.Add(Trader);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Trader.NewPortfolios += portfolios =>
            {
                if (Portfolio != null) return;
                Portfolio = portfolios.FirstOrDefault(p => p.Name == "NL0011100043");
                InitCartAndSrategy();
            };
            Trader.NewSecurities += securities =>
            {
                if (Security != null) return;
                Security = securities.FirstOrDefault(s => s.Id == "SBER@QJSIM");
                InitCartAndSrategy();
            };
            Trader.Connect();
        }

        private void InitCartAndSrategy()
        {
            if (Security == null || Portfolio == null) return;
            Timeframe = TimeSpan.FromSeconds(10);
            CandleManager = new CandleManager(Trader);
            CandleSeries = new CandleSeries(typeof(TimeFrameCandle), Security, Timeframe);

            MyStrategy = new Strategy()
            {
                Connector = Trader,
                Security = Security,
                Volume = 10,
                Portfolio = Portfolio,
                LogLevel = LogLevels.Debug,
            };
            LogManager.Sources.Add(MyStrategy);

            Chart.ClearAreas();
            Area = new ChartArea();
            var yAxis = Area.YAxises.First();
            yAxis.AutoRange = true;
            this.GuiAsync(() =>
            {
                Chart.IsAutoScroll = true;
                Chart.ShowOverview = true;
                Chart.AddArea(Area);
            });
            ChartCandleElement = new ChartCandleElement();
            ChartOrderElement = new ChartOrderElement() { Title = "Ордера" };
            ChartTradeElement = new ChartTradeElement() { Title = "Сделки" };

            Chart.AddElement(Area, ChartCandleElement, CandleSeries);
            Chart.AddElement(Area, ChartOrderElement, Security);
            Chart.AddElement(Area, ChartTradeElement, Security);

            Trader.NewMyTrade += NewMyTrade;
            Trader.NewOrder += NewMyOrder;

            Trader.MarketDepthChanged += MarketDepthControl.UpdateDepth;
            Trader.RegisterMarketDepth(Security);

            CandleManager.Processing += DisplayNewMarketData;
            CandleManager.Start(CandleSeries);

            MyStrategy.Start();
        }

        private void DisplayNewMarketData(CandleSeries candleSeries, Candle candle)
        {
            IsRealTime = candle.OpenTime + Timeframe > DateTimeOffset.Now || IsRealTime;

            var chartDrawData = new ChartDrawData();
            var group = chartDrawData.Group(candle.OpenTime);
            group.Add(ChartCandleElement, candle);

            if (!IsRealTime && candle.State != CandleStates.Finished) return;

            if (!_flag)
            {
                var order = new Order()
                {
                    Type = OrderTypes.Market,
                    Volume = 10,
                    Direction = Sides.Buy,
                };
                order.WhenNewTrade(Trader).Do((trade) =>
                {
                    var takeProfit = new TakeProfitStrategy(trade, 0.5){ QuotingVolume = order.Volume};
                    var stopLoss = new StopLossStrategy(trade, 0.5){ QuotingVolume = order.Volume };
                    var protectiveStrategies = new TakeProfitStopLossStrategy(takeProfit, stopLoss);
                    protectiveStrategies.WhenStopped().Do(() => _flag = false).Apply(MyStrategy);
                    MyStrategy.ChildStrategies.Add(protectiveStrategies);
                }).Apply(MyStrategy);

                MyStrategy.RegisterOrder(order);
                _flag = true;
            }
            Chart.Draw(chartDrawData);
        }

        public void NewMyTrade(MyTrade myTrade)
        {
            var chartDrawData = new ChartDrawData();
            var chartGroup = chartDrawData.Group(myTrade.Trade.Time);
            chartGroup.Add(ChartTradeElement, myTrade);
            Chart.Draw(chartDrawData);
        }

        public void NewMyOrder(Order order)
        {
            var chartDrawData = new ChartDrawData();
            var chartGroup = chartDrawData.Group(order.Time);
            chartGroup.Add(ChartOrderElement, order);
            Chart.Draw(chartDrawData);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            MyStrategy?.Stop();
            MyStrategy?.Dispose();
            CandleManager?.Dispose();
            Trader.Dispose();
            base.OnClosing(e);
        }
    }
}
