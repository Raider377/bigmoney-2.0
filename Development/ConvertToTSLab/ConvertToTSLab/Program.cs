﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;
using System.IO;

namespace ConvertToTSLabFormat
{
    class Program
    {
        private static string inPathData = @"C:\Users\Roman\Desktop\bigmoney-2.0\Data\binance_BTCUSDT_1m.txt";
        private static string outPathData = @"C:\Users\Roman\Desktop\bigmoney-2.0\Data\binance_BTCUSD_1min.txt";

        //private const string delimiter = ",";
        private const int columnNumberDate = 0; private const int columnNumberOpen = 1; private const int columnNumberHigh = 2; private const int columnNumberLow = 3;
        private const int columnNumberClose = 4; private const int columnNumberVol = 5;

        private static List<string> dateTimeList = new List<string>();
        private static List<string> priceOpenList = new List<string>();
        private static List<string> priceHighList = new List<string>();
        private static List<string> priceLowList = new List<string>();
        private static List<string> priceCloseList = new List<string>();
        private static List<string> volumeList = new List<string>();

        private const string ticker = "Binance.BTCUSDT"; private const string per = "1";
        private const string head = "<TICKER>,<PER>,<DATE>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>";

        public static int ColumnNumberDate => columnNumberDate;

        static void Main(string[] args)
        {
            dateTimeList = ReadFile(columnNumberDate);
            priceOpenList = ReadFile(columnNumberOpen);
            priceHighList = ReadFile(columnNumberHigh);
            priceLowList = ReadFile(columnNumberLow);
            priceCloseList = ReadFile(columnNumberClose);
            volumeList = ReadFile(columnNumberVol);

            WriteFile(outPathData);
          
            Console.ReadKey();
        }

        public static List<string> ReadFile(int columnNumber, bool isTxt = true, string inPathData = @"C:\Users\Roman\Desktop\bigmoney-2.0\Data\binance_BTCUSDT_1m.txt", string delimiter = ",")
        {
            List<string> data = new List<string>();

            // Reading file
            try
            {
                using (TextFieldParser parser = new TextFieldParser(inPathData))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(delimiter);
                    while (!parser.EndOfData)
                    {
                        // Processing row
                        string[] fields = parser.ReadFields();
                        data.Add((fields[columnNumber]));
                    }
                }

                if (isTxt)
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return data;
        }

        public static void WriteFile(string outPathData)
        {
            try
            {
                //lines.Add(string.Join(",", fields));
                using (StreamWriter writer = new StreamWriter(outPathData, false))
                {
                    writer.WriteLine(head);
                    for (int i = 0; i < dateTimeList.Count; i++)
                    {
                        var dateTime = TimeStampToDateTime(Convert.ToDouble((dateTimeList[i])));

                        var help = dateTime.Date;
                        string line = ticker + "," + per + "," + dateTime.ToString("yyyyMMdd") + "," + dateTime.ToString("HHmmss") +"," + priceOpenList[i] + "," + priceHighList[i] + "," + priceLowList[i] + "," + priceCloseList[i] + "," + volumeList[i];
                        writer.WriteLine(line);
                    }
                }
                Console.WriteLine("All files are writed successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static DateTime TimeStampToDateTime(double javaTimeStamp)
        {
            // Java timestamp is milliseconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(javaTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}