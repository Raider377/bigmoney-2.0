﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace BigMoney_EMA
{

    public class UnitTestTestConnector
    {
        /*public void TestDBLogger()
        {
            var c = new ConstantForTrade();
            c.AccountName = "TestSendInDB";
            c.InstrumentName = "SRCH";
            c.StrategyNameShort = "TestSendInDB_SRCH";
            c.StrategyPercentStop =
                0.001m;
            c.StrategyPercentProfit = 0.016156m;
            c.StrategyPercentRiskReducer = 0.005836m;
            c.StrategyMinCountOrderForStart = 800;

            c.RiskManagerMaxCountOpenPosition = 5;
            c.RiskManagerPercentLesionOnDay = 0.55m;
            c.RiskManagerStartBalance = 16800;
            c.RiskManagerMaxLoss = -350;

            c.LoggingModTelegram = false;
            c.LoggingModServer = true;
            c.TradingMode = TradingMode.Test;
            c.DesiredOrdersMaxLimit = 6;

            c.ConstraintHourStart = 11;
            c.ConstraintHourCloseAllPositions = 21;
            c.ConstraintHourEnd = 22;

            c.ConnectorReducer = 0.00015m;
            c.ConnectorCurrentBalance = 16800;
            c.ConnectorSlipege = 4;

            var t = new Transaction();
            t.Type = TransactionType.Limit;
            t.Price = "888";
            t.Volume = "1";
            t.Direction = TransactionDirection.Sell;
            t.DateDone = (DateTime.Now).ToShortDateString();
            t.StockStatus = TransactionStockStatus.Done;
            t.TimeDone = (DateTime.Now).ToLongTimeString();
            t.Comment = "";

            var hashPrice = t.Price.GetHashCode();
            var hashStopTake = (c.StrategyPercentStop + c.StrategyPercentProfit).GetHashCode();
            var hashStartBalance = c.RiskManagerStartBalance.GetHashCode();
            t.TransactionStockId = (hashStartBalance + hashPrice + hashStopTake).ToString();
            DataBaseLogger.getInstance().send(c, t);
        }*/
    }



    public static class TradingMode
    {
        public static string Test = "Test";
        public static string Production = "Production";
        public static string Paper = "Paper";
    }

    public static class TransactionType
    {
        public static string Limit = "Limit";
        public static string Stop = "Stop";
        public static string Take = "Take";
        public static string StopOrTake = "StopOrTake";
        public static string Time = "Time";
        public static string Forcibly = "Forcibly";
    }

    public static class TransactionDirection
    {
        public static string Sell = "Sell";
        public static string Buy = "Buy";
    }

    public static class TransactionStockStatus
    {
        public static string Done = "Done";
    }

    public class DataBaseLogger
    {
        private List<string> groupServerIps = new List<string>();
        private static DataBaseLogger instance;

        private DataBaseLogger()
        {
            groupServerIps.Add("http://185.185.68.133:8888/saveTransaction");
            //            groupServerIps.Add("http://127.0.0.1:5000");
        }

        public static DataBaseLogger getInstance()
        {
            if (instance == null)
                instance = new DataBaseLogger();
            return instance;
        }

        public void send(ConstantForTrade constants, Transaction transaction)
        {
            try
            {
                sendAsync(constants, transaction).Wait();
            }
            catch (Exception e)
            {
            }
        }

        private async Task<string> sendAsync(ConstantForTrade constants, Transaction transaction)
        {
            using (var client = new HttpClient())
            {
                JObject payLoad = new JObject(
                    new JProperty("strategyName", constants.getStrategyName()),
                    new JProperty("instrumentName", constants.InstrumentName),
                    new JProperty("tradingMode", constants.TradingMode),
                    new JProperty("tradingAccount", constants.AccountName),
                    new JProperty("strategyParams", constants.getAboutJson()),
                    new JProperty("transactionStockId", transaction.TransactionStockId),
                    new JProperty("transactionPrice", transaction.Price),
                    new JProperty("transactionVolume", transaction.Volume),
                    new JProperty("transactionDirection", transaction.Direction),
                    new JProperty("transactionType", transaction.Type),
                    new JProperty("transactionStockStatus", transaction.StockStatus),
                    new JProperty("transactionDateDone", transaction.DateDone),
                    new JProperty("transactionTimeDone", transaction.TimeDone),
                    new JProperty("transactionComment", transaction.Comment)
                );

                var content = new StringContent(payLoad.ToString(), Encoding.UTF8, "application/json");
                foreach (var serverIp in groupServerIps)
                {
                    var response = await client.PostAsync(serverIp, content).ConfigureAwait(false);
                    var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return responseString;
                }
            }

            return "";
        }
    }


    public class Transaction
    {
        public string Price;
        public string Volume;
        public string Direction;
        public string Type;
        public string StockStatus;
        public string DateDone;
        public string TimeDone;
        public string Comment;

        public string TransactionStockId;
    }

    public class ConstantForTrade
    {
        public string AccountName;
        public string InstrumentName;
        public string StrategyNameShort;

        public bool LoggingModTelegram;
        public bool LoggingModServer;
        public string TradingMode;

        public int PeriodFast;
        public int PeriodSlow;
        public int KTake;
        public int StopValue;
        public int Volume;
        public int timeFrame;

        public string getStrategyName()
        {
            var paramsForHash = PeriodFast + PeriodSlow + KTake;
            var intPart = $"{StopValue}{Volume}{timeFrame}{KTake}{PeriodFast}";
            var hashDouble = paramsForHash.GetHashCode();
            var a =
                $"{StrategyNameShort}_fast{PeriodFast}_slow{PeriodSlow}_KTake{KTake}_StopValue{StopValue}_hash_{hashDouble}{intPart}";
            return a;
        }

        public string getAboutJson()
        {
            var result = "{"
                         + $"\"PeriodFast\":\"{PeriodFast}\"," +
                         $"\"PeriodSlow\":\"{PeriodSlow}\"," +
                         $"\"KTake\":\"{KTake}\"," +
                         $"\"StopValue\":\"{StopValue}\"," +
                         $"\"Volume\":\"{Volume}\"," +
                         $"\"timeFrame\":\"{timeFrame}\"," +
                         $"\"LoggingModTelegram\":\"{LoggingModTelegram}\"," +
                         $"\"LoggingModServer\":\"{LoggingModServer}\"," +
                         "}";

            return result;
        }

        public string getAbout()
        {
            string htmlString = string.Format(
                @"AccountName {0}
PeriodFast {1},
KTake {2},
StopValue {3},
Volume {4},

timeFrame {5},
LoggingModTelegram {6},
LoggingModServer {7}
",
                AccountName,
                PeriodFast,
                KTake,
                StopValue,
                Volume,

                timeFrame,
                LoggingModTelegram,
                LoggingModServer
            );
            return htmlString;
        }
    }
}