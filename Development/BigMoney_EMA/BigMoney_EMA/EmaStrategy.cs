﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.IO;
using System.ComponentModel;
using System.Globalization;

// using S#
using Ecng.Common;
using Ecng.Collections;
using Ecng.ComponentModel;
using Ecng.Configuration;
using Ecng.Data;
using Ecng.Data.Providers;
using Ecng.Interop;
using Ecng.Localization;
using Ecng.Net;
using Ecng.Reflection;
using Ecng.Reflection.Aspects;
using Ecng.Security;
using Ecng.Serialization;
using Ecng.Transactions;
using Ecng.UnitTesting;
using Ecng.Web;
using Ecng.Xaml;
using System.Web;
using MoreLinq;
using StockSharp.Alerts;
using StockSharp.Algo;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Candles.Compression;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Xaml;
using StockSharp.Xaml.Charting;
using StockSharp.Xaml.Diagram;
using StockSharp.Quik;
using StockSharp.Messages;
using StockSharp.Localization;

namespace BigMoney_EMA
{
    public class EmaStrategy : Strategy
    {
        #region Общие переменные
        private readonly CandleSeries _series;
        private readonly MainWindow _mainWnd;
        private readonly QuikTrader _trader;

        // 0 - текущая свеча, 1 - предыдущая
        private const int Index = 0;
        private const int IndexPred = 1;

        private decimal _startPrice;

        private decimal _stopPrice;
        private decimal _takePrice;

        private Order _buyOrder;
        private Order _sellOrder;
        private Order _stopTakeOrder;

        private bool _flagLoadOrders;
        #endregion

        #region Переменные торговой стратегии
        public ExponentialMovingAverage FastEma { get; }
        public ExponentialMovingAverage SlowEma { get; }

        //private const int KTake = 5;
        //private const int StopValue = 250;

        private const int KTake = 1;
        private const int StopValue = 25;
        #endregion

        private ConstantForTrade getConstantInstance()
        {
            var constantForTrade = new ConstantForTrade();

            constantForTrade.AccountName = "SPBFUT00aFq";
            constantForTrade.InstrumentName = "SIM9";
            constantForTrade.StrategyNameShort = "EMA_1";

            constantForTrade.PeriodFast = 1;
            constantForTrade.PeriodSlow = 2;
            constantForTrade.KTake = 1;
            constantForTrade.StopValue = 25;
            constantForTrade.Volume = 1;
            constantForTrade.timeFrame = 1;

            constantForTrade.LoggingModTelegram = true;
            constantForTrade.LoggingModServer = true;
            constantForTrade.TradingMode = TradingMode.Paper;


            return constantForTrade;
        }

        public EmaStrategy(CandleSeries series, QuikTrader trader, MainWindow mainWnd, ExponentialMovingAverage fastEma, ExponentialMovingAverage slowEma)
        {
            _trader = trader;
            _series = series;

            _mainWnd = mainWnd;

            FastEma = fastEma;
            SlowEma = slowEma;

        }

        public void CheckStrategy()
        {
            var checkLine = "";

            if (_buyOrder != null)
            {
                checkLine += " _buyOrder ID: " + _buyOrder.Id + " _buyOrder.Price: " + _buyOrder.Price.ToString(CultureInfo.InvariantCulture)
                    + " _buyOrder.State: " + _buyOrder.State + " _buyOrder.Time: " + _buyOrder.Time;
            }
            if (_buyOrder == null)
                checkLine += " _buyOrder == null ";

            if (_stopTakeOrder != null)
            {
                checkLine += " StopTakeOrder ID: " + _stopTakeOrder.Id + " StopTakeOrder.Price: " + _stopTakeOrder.Price.ToString(CultureInfo.InvariantCulture)
                    + " StopTakeOrder.State: " + _stopTakeOrder.State + " StopTakeOrder.Time: " + _stopTakeOrder.Time;
            }
            if (_stopTakeOrder == null)
                checkLine += " StopTakeOrder == null ";

            if (_sellOrder != null)
            {
                checkLine += " _sellOrder ID: " + _sellOrder.Id + " _sellOrder.Price: " + _sellOrder.Price.ToString(CultureInfo.InvariantCulture)
                    + " _sellOrder.State: " + _sellOrder.State + " _sellOrder.Time: " + _sellOrder.Time;
            }
            if (_sellOrder == null)
                checkLine += " _sellOrder == null ";


            MessageBox.Show(checkLine);
        }

        protected override void OnStarted()
        {
            // Запрашиваем открытые ордера и стоп-заявки.
            GetOpenOrders();


            // Событие завершения каждой свечи.
            _trader
                .WhenCandlesFinished(_series)
                .Do(() =>
                {
                    if (!_mainWnd.IsRealTime)
                    {
                        return;
                    }

                    if (FastEma.GetValue(IndexPred) == 0 || SlowEma.GetValue(IndexPred) == 0 || FastEma.GetValue(Index) == 0 || SlowEma.GetValue(Index) == 0)
                    {
                        return;
                    }

                    // Если нет активных позиций.
                    if (IsNotActivePosition())
                    {
                        // Если прошел сигнал на покупку.
                        if (IsSignalBuy())
                        {
                            // Открываем позицию в лонг.
                            OpenLong(Volume);
                        }
                        // Если прошел сигнал на продажу.
                        else if (IsSignalSell())
                        {
                            // Открываем позицию в шорт.
                            OpenShort(Volume);
                        }
                    }
                    else
                    {
                        // Если открыта позиция лонг и прошел сигнал на продажу.
                        if (IsActivePositionLong() && IsSignalSell())
                        {
                            // Отменяем стоп-лимит ордер.
                            CancelStopOrder();
                            // Обнуляем ордер на покупку.
                            _buyOrder = null;

                            // Переворачиваемся в шорт.
                            OpenShort(2 * Volume);
                        }
                        // Если открыта позиция шорт и прошел сигнал на покупку.
                        else if (IsActivePositionShort() && IsSignalBuy())
                        {
                            // Отменяем стоп-лимит ордер.
                            CancelStopOrder();

                            // Обнуляем ордер на продажу.
                            _sellOrder = null;

                            // Переворачиваемся в лонг.
                            OpenLong(2 * Volume);
                        }
                    }

                })
                .Apply(this);


            base.OnStarted();
        }

        private void GetOpenOrders()
        {
            if (_flagLoadOrders) return;
            if (_trader == null) return;

            var targetPosition = _trader.Positions.FirstOrDefault(p => p.Security == Security);

            Position = targetPosition == null ?
                0 : (targetPosition.CurrentValue ?? 0);

            if (Position != 0)
            {
                var orders = _trader.Orders.OrderByDescending(x => x.LastChangeTime);
                foreach (var order in orders)
                {
                    if (_buyOrder != null && _stopTakeOrder != null || _sellOrder != null && _stopTakeOrder != null)
                    {
                        break;
                    }

                    if (order.Type == OrderTypes.Limit)
                    {
                        if (order.Direction == Sides.Buy && _buyOrder == null && Position > 0)
                        {
                            _buyOrder = order;
                        }
                        else
                        {
                            if (order.Direction == Sides.Sell && _sellOrder == null && Position < 0)
                            {
                                _sellOrder = order;
                            }
                        }
                    }

                    if (order.Type == OrderTypes.Conditional && _stopTakeOrder == null)
                    {
                        _stopTakeOrder = order;
                        _stopTakeOrder.State = OrderStates.Active;

                        if (order.Direction == Sides.Buy)
                        {
                            OnStopOrderCheckActivateBuy();
                        }
                        else
                        {
                            OnStopOrderCheckActivateShort();
                        }
                    }
                }
            }
            _flagLoadOrders = true;
        }

        private bool IsNotActivePosition()
        {
            return _buyOrder == null && _sellOrder == null && _stopTakeOrder == null;
        }

        private bool IsSignalBuy()
        {
            return FastEma.GetValue(IndexPred) <= SlowEma.GetValue(IndexPred) && FastEma.GetValue(Index) > SlowEma.GetValue(Index);
        }

        private bool IsSignalSell()
        {
            return FastEma.GetValue(IndexPred) >= SlowEma.GetValue(IndexPred) && FastEma.GetValue(Index) < SlowEma.GetValue(Index);
        }

        private bool IsActivePositionLong()
        {
            return _buyOrder != null && _sellOrder == null && _stopTakeOrder != null;
        }

        private bool IsActivePositionShort()
        {
            return _buyOrder == null && _sellOrder != null && _stopTakeOrder != null;
        }

        /// <summary>
        /// Отмена стоп-лимит ордера и обработка событий по ордеру.
        /// </summary>
        private void CancelStopOrder()
        {
            #region Создаем правило - ошибка снятия стоп заявки

            _stopTakeOrder.WhenCancelFailed(_trader)
                .Do(() =>
                {
                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _trader.UnRegisterMarketDepth(Security);
                        _mainWnd.Strategy.Stop();
                        _mainWnd.StartButton.Content = LocalizedStrings.Str2421;

                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "НЕ МОГУ снять стоп-лимит заявку. Стратегия отключена.");
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));
                })
                .Apply(this);

            #endregion

            _trader.CancelOrder(_stopTakeOrder);
            _stopTakeOrder = null;
        }

        /// <summary>
        /// Регистрация стоп-лимит ордера и обработка событий по ордеру.
        /// </summary>
        private void RegisterStopOrder(bool isLong)
        {
            // Получаем цены стоп и тейк.
            if (isLong)
            {
                _stopPrice = _startPrice - StopValue;
                _takePrice = _startPrice + KTake * StopValue;
            }
            else
            {
                _stopPrice = _startPrice + StopValue;
                _takePrice = _startPrice - KTake * StopValue;
            }

            // Создаем стоп-лимит ордер.
            _stopTakeOrder = new Order
            {
                Type = OrderTypes.Conditional,
                Volume = base.Volume,
                Price = _stopPrice,
                Security = base.Security,
                Portfolio = base.Portfolio,
                ExpiryDate = DateTime.MaxValue,
                Direction = isLong ? Sides.Sell : Sides.Buy,
                Condition = new QuikOrderCondition
                {
                    Type = QuikOrderConditionTypes.TakeProfitStopLimit,
                    StopPrice = _takePrice,
                    StopLimitPrice = _stopPrice,
                    Offset = new Unit(0),
                    Spread = new Unit(0),
                },
            };

            #region Правило на случай ошибки регистрации стоп-лимит заявки
            _stopTakeOrder.WhenRegisterFailed(_trader)
            .Do(() =>
            {
                _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                {
                    CancelActiveOrders();

                    _trader.UnRegisterMarketDepth(Security);
                    _mainWnd.Strategy.Stop();
                    _mainWnd.StartButton.Content = LocalizedStrings.Str2421;

                    _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать стоп-лимит ордер. Стратегия отключена. Все заявки отменены");
                    _mainWnd.LogWindow.ScrollToEnd();
                }));
            })
            .Apply(this);
            #endregion

            #region Правило на случай успешной регистрации стоп-заявки
            _stopTakeOrder
            .WhenRegistered(_trader)
            .Do(() =>
            {
                _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                {
                    _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Стоп-лимит ордер зарегистрирован: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", стоп-цена: " + _stopPrice.ToString(CultureInfo.InvariantCulture) + ", тейк-цена: " + _takePrice.ToString(CultureInfo.InvariantCulture));
                    _mainWnd.LogWindow.ScrollToEnd();
                }));
            })
            .Apply(this);
            #endregion

            if (isLong)
            {
                OnStopOrderCheckActivateBuy();
            }
            else
            {
                OnStopOrderCheckActivateShort();
            }


            // Регистрируем стоп-лимит ордер.
            _trader.RegisterOrder(_stopTakeOrder);
        }

        private void OnStopOrderCheckActivateBuy()
        {
            #region Правило на случай срабатывания стоп-ордера
            _stopTakeOrder
                .WhenMatched(_trader)
                .Do(() =>
                {
                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по стоп-лимиту: " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));

                    var stopTakeTransact = new Transaction();
                    stopTakeTransact.Price = MainWindow.currentMiddlePriceMD.ToString();
                    stopTakeTransact.Volume = "1"; //обрати внимание тут потенциальный баг
                    stopTakeTransact.Direction = TransactionDirection.Buy;
                    stopTakeTransact.DateDone = (DateTime.Now).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                    stopTakeTransact.StockStatus = TransactionStockStatus.Done;
                    stopTakeTransact.TimeDone = (DateTime.Now).ToLongTimeString();
                    stopTakeTransact.Comment = "";
                    stopTakeTransact.TransactionStockId = _stopTakeOrder.TransactionId.ToString();
                    stopTakeTransact.Type = TransactionType.StopOrTake;


                    DataBaseLogger.getInstance().send(getConstantInstance(), stopTakeTransact);

                    _buyOrder = null;

                    _sellOrder = null;

                    _stopTakeOrder = null;

                })
                .Apply(this);
            #endregion
        }

        private void OnStopOrderCheckActivateShort()
        {
            #region Правило на случай срабатывания стоп-ордера
            _stopTakeOrder
                .WhenMatched(_trader)
                .Do(() =>
                {
                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по стоп-лимиту: " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));

                    var stopTakeTransact = new Transaction();
                    stopTakeTransact.Price = MainWindow.currentMiddlePriceMD.ToString();
                    stopTakeTransact.Volume = "1"; //обрати внимание тут потенциальный баг
                    stopTakeTransact.Direction = TransactionDirection.Sell;
                    stopTakeTransact.DateDone = (DateTime.Now).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                    stopTakeTransact.StockStatus = TransactionStockStatus.Done;
                    stopTakeTransact.TimeDone = (DateTime.Now).ToLongTimeString();
                    stopTakeTransact.Comment = "";
                    stopTakeTransact.TransactionStockId = _stopTakeOrder.TransactionId.ToString();
                    stopTakeTransact.Type = TransactionType.StopOrTake;


                    DataBaseLogger.getInstance().send(getConstantInstance(), stopTakeTransact);

                    _buyOrder = null;

                    _sellOrder = null;

                    _stopTakeOrder = null;

                })
                .Apply(this);
            #endregion
        }

        /// <summary>
        /// Логика открытия позиции в лонг.
        /// </summary>
        private void OpenLong(decimal volume)
        {
            #region Создаем заявку на покупку

            _buyOrder = new Order
            {
                Type = OrderTypes.Market,
                Price = Security.BestAsk.Price,
                Portfolio = Portfolio,
                Security = Security,
                Volume = volume,
                Direction = Sides.Buy,
            };

            #endregion

            #region Создаем правило на случай ошибки регистрации заявки

            _buyOrder.WhenRegisterFailed(_trader)
                .Do(() =>
                {
                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.Strategy.CancelActiveOrders();

                        _trader.UnRegisterMarketDepth(Security);
                        _mainWnd.Strategy.Stop();
                        _mainWnd.StartButton.Content = LocalizedStrings.Str2421;

                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на покупку. Стратегия отключена. Все заявки отменены");
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));
                })
                .Apply(this);

            #endregion

            #region Создаем правило успешного исполнения ордера 

            _buyOrder
                .WhenMatched(_trader)
                .Do(() =>
                {
                    _startPrice = _buyOrder.Price;

                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Заявка на покупку исполнена: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + " по цене: " + Convert.ToInt32(_startPrice));
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));

                    // Регистрируем стоп-ордер и подписываемся на события.
                    RegisterStopOrder(true);


                    var sellTransact = new Transaction();
                    sellTransact.Type = TransactionType.Limit;
                    sellTransact.Price = _startPrice.ToString(CultureInfo.InvariantCulture);
                    sellTransact.Volume = "1"; //обрати внимание тут потенциальный баг
                    sellTransact.Direction = TransactionDirection.Buy;
                    sellTransact.DateDone = (DateTime.Now).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                    sellTransact.StockStatus = TransactionStockStatus.Done;
                    sellTransact.TimeDone = (DateTime.Now).ToLongTimeString();
                    sellTransact.Comment = "";
                    sellTransact.TransactionStockId = _buyOrder.TransactionId.ToString();
                    DataBaseLogger.getInstance().send(getConstantInstance(), sellTransact);

                })
                .Apply(this);
            #endregion

            // Регистрируем ордер на покупку.
            _trader.RegisterOrder(_buyOrder);
        }

        /// <summary>
        /// Логика открытия позиции в шорт.
        /// </summary>
        private void OpenShort(decimal volume)
        {
            #region Создаем заявку на продажу

            _sellOrder = new Order
            {
                Type = OrderTypes.Market,
                Price = Security.BestBid.Price,
                Portfolio = Portfolio,
                Security = Security,
                Volume = volume,
                Direction = Sides.Sell,
            };

            #endregion

            #region Создаем правило на случай ошибки регистрации заявки

            _sellOrder.WhenRegisterFailed(_trader)
                .Do(() =>
                {
                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.Strategy.CancelActiveOrders();

                        _trader.UnRegisterMarketDepth(Security);
                        _mainWnd.Strategy.Stop();
                        _mainWnd.StartButton.Content = LocalizedStrings.Str2421;

                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на продажу. Стратегия отключена. Все заявки отменены");
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));
                })
                .Apply(this);

            #endregion

            #region Создаем правило успешного исполнения ордера 

            _sellOrder
                .WhenMatched(_trader)
                .Do(() =>
                {
                    _startPrice = _sellOrder.Price;

                    _mainWnd.Dispatcher.BeginInvoke(new Action(delegate
                    {
                        _mainWnd.LogWindow.AppendText(System.Environment.NewLine + "Заявка на продажу исполнена: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + " по цене: " + Convert.ToInt32(_startPrice));
                        _mainWnd.LogWindow.ScrollToEnd();
                    }));

                    var sellTransact = new Transaction();
                    sellTransact.Type = TransactionType.Limit;
                    sellTransact.Price = _startPrice.ToString(CultureInfo.InvariantCulture);
                    sellTransact.Volume = "1"; //обрати внимание тут потенциальный баг
                    sellTransact.Direction = TransactionDirection.Sell;
                    sellTransact.DateDone = (DateTime.Now).ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                    sellTransact.StockStatus = TransactionStockStatus.Done;
                    sellTransact.TimeDone = (DateTime.Now).ToLongTimeString();
                    sellTransact.Comment = "";
                    sellTransact.TransactionStockId = _sellOrder.TransactionId.ToString();
                    DataBaseLogger.getInstance().send(getConstantInstance(), sellTransact);

                    // Регистрируем стоп-ордер и подписываемся на события.
                    RegisterStopOrder(false);
                })
                .Apply(this);
            #endregion

            // Регистрируем ордер на продажу.
            _trader.RegisterOrder(_sellOrder);
        }
    }

}