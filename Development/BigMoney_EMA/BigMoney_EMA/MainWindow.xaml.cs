﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.IO;
using System.Web;
using System.Threading;

// using S#
using Ecng.Common;
using Ecng.Collections;
using Ecng.ComponentModel;
using Ecng.Configuration;
using Ecng.Data;
using Ecng.Data.Providers;
using Ecng.Interop;
using Ecng.Localization;
using Ecng.Net;
using Ecng.Reflection;
using Ecng.Reflection.Aspects;
using Ecng.Security;
using Ecng.Serialization;
using Ecng.Transactions;
using Ecng.UnitTesting;
using Ecng.Web;
using Ecng.Xaml;

using StockSharp.Algo.Storages.Csv;
using StockSharp.Alerts;
using StockSharp.Algo;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Candles.Compression;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Strategies.Reporting;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Xaml;
using StockSharp.Xaml.Charting;
using StockSharp.Xaml.Diagram;
using StockSharp.Quik;
using StockSharp.Messages;
using StockSharp.Localization;

using System.Diagnostics;
using System.ComponentModel;
using System.Globalization;

using MoreLinq;

using Ookii.Dialogs.Wpf;
using StockSharp.Configuration;

namespace BigMoney_EMA
{
    public partial class MainWindow
    {

        public List<MyTrade> TradeList;
        public List<Order> OrderList;

        private QuikTrader _trader; // коннектор к quik

        private readonly TimeSpan _timeFrame = TimeSpan.FromMinutes(1); // таймфрейм
        private const int Volume = 1; // рабочий объём

        public EmaStrategy Strategy;
        private Security _security; // инструмент
                                    // private Portfolio _portfolio; // портфель

        private CandleSeries _series; // поток свечей
        private readonly ChartArea _area; // область графика
        private ChartCandleElement _candleElem; // элемент области графика

        //private DateTimeOffset _from, _to;

        private const string MySecurity = "SIM9";
        private Portfolio _portfolio; // портфель

        private ChartIndicatorElement _fastEmaElem;
        private ChartIndicatorElement _slowEmaElem;
       
        public int PeriodFast = 1;
        public int PeriodSlow = 2;
        //public int PeriodFast = 20;
        //public int PeriodSlow = 90;

        public bool IsRealTime;
        private CandleManager _candleManager;
        //private bool _isTodayEmaDrawn = false;

        private const string SettingsFileConnection = "connection.xml";
        private const string SettingsFileStrategy = "strategy.xml";

        private ChartTradeElement _tradesElem;
        private ChartOrderElement _ordersElem;

        public static decimal currentMiddlePriceMD = 0m;

        public MainWindow()
        {
            InitializeComponent();

            _chart.ChartTheme = ChartThemes.ExpressionDark;

            // Добавляем область графика
            _area = new ChartArea();
            _chart.Areas.Add(_area);

            // Находим путь к Quik
            quikPath.Text = QuikTerminal.GetDefaultPath();
        }

        private void NewMyTrade(MyTrade myTrade)
        {
            var chartDrawData = new ChartDrawData();
            var chartGroup = chartDrawData.Group(myTrade.Trade.Time);

            chartGroup.Add(_tradesElem, myTrade);
            _chart?.Draw(chartDrawData);
        }

        private void NewMyOrder(Order order)
        {
            var chartDrawData = new ChartDrawData();
            var chartGroup = chartDrawData.Group(order.Time);

            chartGroup.Add(_ordersElem, order);
            _chart?.Draw(chartDrawData);
        }


        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (_trader == null || _trader.ConnectionState == ConnectionStates.Disconnected)
            {
                if (_trader == null)
                {
                    if (quikPath.Text.IsEmpty())
                    {
                        MessageBox.Show(this, LocalizedStrings.Str2983);
                        return;
                    }

                    // создаем подключение
                    _trader = new QuikTrader(quikPath.Text);

                    // Пытаемся подгрузить файл с настройками.
                    try
                    {
                        if (File.Exists(SettingsFileConnection))
                            _trader.Load(new XmlSerializer<SettingsStorage>().Deserialize(SettingsFileConnection));
                    }
                    catch
                    {
                        // ignored
                    }

                    // инициализируем механизм переподключения (будет автоматически соединяться
                    // каждые 10 секунд, если шлюз потеряется связь с сервером)
                    _trader.ReConnectionSettings.Interval = TimeSpan.FromSeconds(10);

                    // переподключение будет работать только во время работы биржи РТС
                    // (чтобы отключить переподключение когда торгов нет штатно, например, ночью)
                    _trader.ReConnectionSettings.WorkingTime = ExchangeBoard.Forts.WorkingTime;


                    Portfolios.Portfolios = new PortfolioDataSource(_trader);


                    _trader.Connected += () =>
                    {
                        _candleManager = new CandleManager(_trader);

                        _trader.NewSecurity += security =>
                        {
                            if (!security.Code.CompareIgnoreCase(MySecurity))
                            {
                                return;
                            }
                            _security = security;

                            this.GuiAsync(() =>
                            {
                                StartButton.IsEnabled = true;
                            });
                        };


                        _trader.MarketDepthsChanged += (depths) =>
                        {
                            foreach (var depth in depths)
                            {
                                var bestBidPrice = depth.BestBid.Price;
                                var bestAskPrice = depth.BestAsk.Price;
                                var hour = depth.LastChangeTime.Hour;
                                var simple = (bestBidPrice + bestAskPrice) / 2;
                                currentMiddlePriceMD = simple;
                            }
                        };
                    

                    _trader.ConnectionError += ex =>
                        {
                            if (ex != null)
                            {
                                this.GuiAsync(() => MessageBox.Show(this, ex.ToString()));
                            }
                        };

                        this.GuiAsync(() =>
                        {
                            ConnectButton.IsEnabled = false;
                            ReportButton.IsEnabled = true;
                            CheckButton.IsEnabled = true;
                        });

                        /* _candleManager.Processing += (series, candle) =>
                              {
                                      // если скользящие за сегодняшний день отрисованы, то рисуем в реальном времени текущие скользящие
                                           /*if (_isTodayEmaDrawn && candle.State == CandleStates.Finished)
                            { }*/
                        // DrawCandle(series, candle);
                        //  };
                    };
                }
                _trader.Connect();
            }
            else
            {
                _trader.Disconnect();
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (Strategy == null)
            {
                if (Portfolios.SelectedPortfolio == null)
                {
                    MessageBox.Show(this, LocalizedStrings.Str3009);
                    return;
                }

                // Создаем поток свечей и указываем что тип TimeFrameCandle.
                _series = new CandleSeries(typeof(TimeFrameCandle), _security, _timeFrame);

                // Создание элемента графика представляющего свечки и добавление его в область графика.
                _candleElem = new ChartCandleElement
                {
                    AntiAliasing = false,
                    UpFillColor = Colors.White,
                    UpBorderColor = Colors.Black,
                    DownFillColor = Colors.Black,
                    DownBorderColor = Colors.Black,
                };
                _area.Elements.Add(_candleElem);

                // Добавление элементов индикатора на график.
                _slowEmaElem = new ChartIndicatorElement
                {
                    FullTitle = LocalizedStrings.Long,
                    Color = Colors.OrangeRed
                };
                _area.Elements.Add(_slowEmaElem);

                _fastEmaElem = new ChartIndicatorElement
                {
                    FullTitle = LocalizedStrings.Short,
                    Color = Colors.RoyalBlue
                };
                _area.Elements.Add(_fastEmaElem);

                _tradesElem = new ChartTradeElement { FullTitle = LocalizedStrings.Str985 };
                _area.Elements.Add(_tradesElem);

                _ordersElem = new ChartOrderElement { FullTitle = "Ордера" };
                _area.Elements.Add(_ordersElem);


                // Создаем торговую стратегию.
                Strategy = new EmaStrategy(_series, _trader, this, new ExponentialMovingAverage { Length = PeriodFast }, new ExponentialMovingAverage { Length = PeriodSlow })
                {
                    Security = _security,
                    Portfolio = Portfolios.SelectedPortfolio,
                    Connector = _trader,
                    Volume = Volume
                };

                /////////////////////////
                if (File.Exists(SettingsFileStrategy))
                {
                    //Загрузка настроек стратегии из существующего конфигурационного файла.
                    Strategy.Load(new XmlSerializer<SettingsStorage>().Deserialize(SettingsFileStrategy));
                }
                /////////////////////////

                // подписываемся на события
                Strategy.Log += OnLog;
                Strategy.PropertyChanged += OnStrategyPropertyChanged;


                TradeList = new List<MyTrade>();
                OrderList = new List<Order>();

                _trader.NewOrder += order => this.GuiAsync(() =>
                {
                    //OrderList.Add(myOrder);
                    Orders.Orders.Add(order);
                    NewMyOrder(order);
                });

                _trader.NewMyTrade += myTrade => this.GuiAsync(() =>
                {
                    //TradeList.Add(myTrade);
                    Trades.Trades.Add(myTrade);
                    NewMyTrade(myTrade);

                });

                // подгрузка истории
                IEnumerable<Candle> candles = CultureInfo.InvariantCulture.DoInCulture(() => File.ReadAllLines("SiZ8_history.txt").Select(line =>
                {
                    var parts = line.Split(',');
                    var time = (parts[0] + parts[1]).ToDateTime("yyyyMMddHHmmss").ApplyTimeZone(TimeHelper.Moscow);
                    return (Candle)new TimeFrameCandle
                    {
                        OpenPrice = parts[2].To<decimal>(),
                        HighPrice = parts[3].To<decimal>(),
                        LowPrice = parts[4].To<decimal>(),
                        ClosePrice = parts[5].To<decimal>(),
                        TimeFrame = _timeFrame,
                        OpenTime = time,
                        CloseTime = time + _timeFrame,
                        TotalVolume = parts[6].To<decimal>(),
                        Security = _security,
                        State = CandleStates.Finished,
                    };
                }).ToArray());

                // var lastCandleTime = default(DateTimeOffset);

                // прорисовываем загруженные свечи
                foreach (var candle in candles)
                {
                    DrawCandle(_series, candle);
                    // lastCandleTime = candle.OpenTime;
                }
                /*
                 * var trades = new TradeFromTxt(@"D:\trades_RIZ2@RTS_2012_11_08.txt", _instr1).GetTrades();
 
var storageRegistry = new StorageRegistry();
((LocalMarketDataDrive)storageRegistry.DefaultDrive).Path = @"D:\";
storageRegistry.GetTradeStorage(_instr1).Save(trades);
 
_candleManager = new CandleManager(_trader);
_candleManager.Processing += ProcessCandle;
 
var source = new TradeStorageCandleBuilderSource { StorageRegistry = storageRegistry };
_candleManager.Sources.OfType<TimeFrameCandleBuilder>().Single().Sources.Add(source);
 
_series = new CandleSeries(typeof(TimeFrameCandle), _instr1, _timeFrame);
 
_candleManager.Start(_series, new DateTime(2012, 11, 8), DateTime.MaxValue);
_trader.AddInfoLog("Запуск получения свечек");
                 */
                _candleManager.Start(_series);

                // вычисляем временные отрезки текущей свечи
                // var bounds = _timeFrame.GetCandleBounds(_trader.CurrentTime);
                //candles = _candleManager.Container.GetCandles(_series, new Range<DateTimeOffset>(lastCandleTime + _timeFrame, bounds.Min));

                //_from = _timeFrame.Ticks == 1 ? DateTime.Today : DateTime.Now.Subtract(TimeSpan.FromTicks(_timeFrame.Ticks * 10000));
                _trader.CandleSeriesProcessing += DrawCandle;
                // _trader.SubscribeCandles(_series, _from, _trader.CurrentTime);

                /* foreach (var candle in candles)
                 {
                     DrawCandle(_series, candle);
                 }*/

                // _isTodayEmaDrawn = true;

                //////////
                /*if (lastCandleTime != default(DateTimeOffset))
                {
                    var val = _timeFrame.Ticks == 1 ? DateTime.Today : DateTime.Now.Subtract(TimeSpan.FromTicks(_timeFrame.Ticks * 10000));
                    _from = val < lastCandleTime ? val : lastCandleTime;
                }*/
                ///////

                // прорисывываем свечи

                //_from = _timeFrame.Ticks == 1 ? DateTime.Today : DateTime.Now.Subtract(TimeSpan.FromTicks(_timeFrame.Ticks * 10000));
                //_trader.CandleSeriesProcessing += DrawCandle;
                //_trader.SubscribeCandles(_series, _from, null);


                ReportButton.IsEnabled = true;
            }

            if (Strategy.ProcessState == ProcessStates.Stopped)
            {
                // запускаем процесс получения стакана
                _trader.RegisterMarketDepth(Strategy.Security);
                Strategy.Start();
                StartButton.Content = LocalizedStrings.Str242;
            }
            else
            {
                _trader.UnRegisterMarketDepth(Strategy.Security);
                Strategy.Stop();
                StartButton.Content = LocalizedStrings.Str2421;
            }
        }

        private void DrawCandle(CandleSeries series, Candle candle)
        {
            IsRealTime = candle.OpenTime + _timeFrame > DateTimeOffset.Now || IsRealTime;

            var fastEmaValue = candle.State == CandleStates.Finished ? Strategy.FastEma.Process(candle.OpenPrice) : null;
            var slowEmaValue = candle.State == CandleStates.Finished ? Strategy.SlowEma.Process(candle.OpenPrice) : null;

            var chartData = new ChartDrawData();
            if (series != _series) return;

            chartData
                .Group(candle.OpenTime)
                .Add(_candleElem, candle)
                .Add(_slowEmaElem, slowEmaValue)
                .Add(_fastEmaElem, fastEmaValue);

            _chart.Draw(chartData);
        }

        private void OnStrategyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.GuiAsync(() =>
            {
                Status.Content = Strategy.ProcessState;
                Position.Content = Strategy.Position;
            });
        }


        private void CheckButton_Click(object sender, RoutedEventArgs e)
        {
            Strategy?.CheckStrategy();
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            // сгерерировать отчет по прошедшему тестированию
            new ExcelStrategyReport(Strategy, "report.xlsx").Generate();

            // открыть отчет
            Process.Start("report.xlsx");
        }

        private void FindPathButton_Click(object sender, RoutedEventArgs e)
        {
            // Находим путь к запущенному Quik.
            var dialog = new VistaFolderBrowserDialog();

            if (!quikPath.Text.IsEmpty())
            {
                dialog.SelectedPath = quikPath.Text;

            }
            if (dialog.ShowDialog(this) == true)
            {
                quikPath.Text = dialog.SelectedPath;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // Подтверждение выхода.
            if (MessageBox.Show(this, "Уверены?", "Подтверждение выхода", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                // Отменяем закрытие.
                e.Cancel = true;
            }
            else
            {
                // Сохраняем настройки, закрываем программу.
                if (_trader != null) new XmlSerializer<SettingsStorage>().Serialize(_trader.Save(), SettingsFileConnection);
                if (Strategy != null) new XmlSerializer<SettingsStorage>().Serialize(Strategy.Save(), SettingsFileStrategy);

                _trader?.Disconnect();
                _trader?.Dispose();
            }

            base.OnClosing(e);
        }

        private void OnLog(LogMessage message)
        {
            // Обрабатываем второстепенные сообщений.
            if (message.Level != LogLevels.Info && message.Level != LogLevels.Debug)
            {
                this.GuiAsync(() => MessageBox.Show(this, message.Message));
            }
        }
        private void OrdersOrderSelected()
        {
            CancelOrdersButton.IsEnabled = !Orders.SelectedOrders.IsEmpty();
        }

        private void CancelOrdersButton_Click(object sender, RoutedEventArgs e)
        {
            Orders.SelectedOrders.ForEach(_trader.CancelOrder);
        }
    }
}