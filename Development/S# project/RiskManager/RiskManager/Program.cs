﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Ecng.Common;
using StockSharp.Algo;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Messages;
using StockSharp.Quik;
using StockSharp.Localization;

namespace RiskManager
{
    class Program
    {
        private static Portfolio _portfolio;
        private static string account = "76557ud";
        private static IEnumerable<Position> _positions = null;
        //private static string quikPath = @"D:\Program Files\QuikFinam";

        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    // Находим путь к терминалу
                    var quikPath = QuikTerminal.GetDefaultPath();
                    var terminal = QuikTerminal.Get(@"D:\Program Files\QuikFinam");

                    if (quikPath.IsEmpty())
                    {
                        Console.WriteLine(LocalizedStrings.Str2984);
                        return;
                        // Пытаемся запустить терминал
                        /* var terminal = QuikTerminal.Get(@"D:\Program Files\QuikFinam");
                         if (!terminal.IsLaunched)
                         {
                             Console.WriteLine(LocalizedStrings.QuikStarting);

                             terminal.Launch();
                             Console.WriteLine(LocalizedStrings.QuikLaunched);
                         }
                         else
                         {
                             Console.WriteLine(LocalizedStrings.QuikFound);
                         }

                         // Пытаемся подключиться к сервису торгов
                         if (!terminal.IsConnected)
                         {
                             terminal.Login(login, password);
                             Console.WriteLine(LocalizedStrings.AuthorizationSuccessful);
                         }*/
                    }

                    Console.WriteLine(LocalizedStrings.Str2985, quikPath);


                    using (var waitHandle = new AutoResetEvent(false))
                    {
                        // создаем подключение к Quik-у
                        using (var trader = new QuikTrader(quikPath) { })
                        {
                            // подписываемся на событие успешного подключения
                            // все действия необходимо производить только после подключения
                            trader.Connected += () =>
                            {
                                Console.WriteLine(LocalizedStrings.Str2169);

                                // извещаем об успешном соединени
                                waitHandle.Set();
                            };

                            Console.WriteLine(LocalizedStrings.Str2170);


                            trader.Connect();
                            waitHandle.WaitOne();

                            trader.NewPortfolio += portfolio =>
                            {
                                if (_portfolio == null && portfolio.Name == account)
                                {
                                    // находим нужный портфель и присваиваем его переменной _portfolio
                                    _portfolio = portfolio;

                                    if (_portfolio != null)
                                    {
                                        waitHandle.Set();
                                    }
                                    Console.WriteLine(LocalizedStrings.Str2171Params, account);
                                }
                            };

                            waitHandle.WaitOne();
                            trader.NewPositions += positions =>
                            {
                                if (_positions == null)
                                {
  
                                    _positions = positions;

                                    if (_portfolio != null)
                                    {
                                        waitHandle.Set();
                                    }
                                    Console.WriteLine("Позиции по сделкам найдены!");
                                }
                            };

                            //Console.WriteLine(LocalizedStrings.Str2989Params.Put(account));
                            waitHandle.WaitOne();
                            while (true)
                            {
                                // Проверка риска не день
                                var marg = _portfolio.VariationMargin;
                                if (marg <= -100)
                                {
                                    terminal.Logout();
                                    Console.WriteLine(LocalizedStrings.QuikDisconnected);

                                    terminal.Exit();
                                    Console.WriteLine(LocalizedStrings.Str3008);
                                    Console.WriteLine("ПИЗДУЙ ОТДЫХАТЬ!!!!");
                                    break;
                                }
                                
                                var Positions = trader.Positions.ToList();
                                var timeChangedLastPositions = Positions[Positions.Count - 1].LastChangeTime.AddMinutes(60);
                               
                                
                                if ((Positions[Positions.Count - 1].VariationMargin < 0) && (timeChangedLastPositions >= terminal.ServerTime.Value))
                                {
                                    terminal.Logout();
                                    Console.WriteLine(LocalizedStrings.QuikDisconnected);

                                    terminal.Exit();
                                    Console.WriteLine(LocalizedStrings.Str3008);
                                    Console.WriteLine("ПИЗДУЙ ОТДЫХАТЬ!!!!");
                                    break;
                                }
                            }

                            // останавливаем подключение
                            trader.Disconnect();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadKey();
        }
    }
}
