#region S# License
/******************************************************************************************
NOTICE!!!  This program and source code is owned and licensed by
StockSharp, LLC, www.stocksharp.com
Viewing or use of this code requires your acceptance of the license
agreement found at https://github.com/StockSharp/StockSharp/blob/master/LICENSE
Removal of this comment is a violation of the license agreement.

Project: SampleConsole.SampleConsolePublic
File: Program.cs
Created: 2015, 11, 11, 2:32 PM

Copyright 2010 by StockSharp, LLC
*******************************************************************************************/
#endregion S# License
namespace SampleConsole
{
    using System;
    using System.Threading;

    using Ecng.Common;

    using StockSharp.Algo;
    using StockSharp.Algo.Candles;
    using StockSharp.Algo.Strategies.Reporting;
    using StockSharp.Algo.Indicators;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    using StockSharp.Messages;
    using StockSharp.Quik;
    using StockSharp.Localization;

    class Program
    {
        private static Security _sber;
        private static Portfolio _portfolio;
        private static MarketDepth _depth;

        public const int VolumeDensity = 5;

        static void Main()
        {
            try
            {
                // для теста выбираем бумагу Лукойл
                const string secCode = "SIZ8";

                var quikPath = QuikTerminal.GetDefaultPath();

                if (quikPath.IsEmpty())
                {
                    Console.WriteLine(LocalizedStrings.Str2984);
                    return;
                }

                Console.WriteLine(LocalizedStrings.Str2985 + quikPath);

                //Console.Write(LocalizedStrings.Str2986);
                //var account = Console.ReadLine();
                var account = "76557ud";

                using (var waitHandle = new AutoResetEvent(false))
                {
                    // создаем подключение к Quik-у
                    using (var trader = new QuikTrader(quikPath) { })
                    {
                        // необходимо раскомментировать, если идет работа с РТС Стандарт
                        //trader.FormatTransaction += builder => builder.RemoveInstruction(Transaction.TimeInForce);

                        // подписываемся на событие успешного подключения
                        // все действия необходимо производить только после подключения
                        trader.Connected += () =>
                        {
                            Console.WriteLine(LocalizedStrings.Str2169);

                            // извещаем об успешном соединени
                            waitHandle.Set();
                        };

                        Console.WriteLine(LocalizedStrings.Str2170);

                        /*trader.DdeTables = new[] { trader.SecuritiesTable, trader.MyTradesTable, trader.EquityPositionsTable,
                                           trader.EquityPortfoliosTable, trader.OrdersTable };*/


                        trader.Connect();

                        // дожидаемся события об успешном соединении
                        waitHandle.WaitOne();

                        trader.NewPortfolio += portfolio =>
                        {
                            if (_portfolio == null && portfolio.Name == account)
                            {
                                // находим нужный портфель и присваиваем его переменной _portfolio
                                _portfolio = portfolio;

                                Console.WriteLine(LocalizedStrings.Str2171Params, account);

                                // если инструмент и стакан уже появились,
                                // то извещаем об этом основной поток для выставления заявки
                                if (_sber != null && _depth != null)
                                    waitHandle.Set();
                            }
                        };

                        // подписываемся на событие появление инструментов
                        trader.NewSecurity += security =>
                        {
                            if (_sber == null)
                            {
                                if (!security.Code.CompareIgnoreCase(secCode))
                                    return;

                                // находим Лукойл и присваиваем ее переменной lkoh
                                _sber = security;

                                if (_sber != null)
                                {
                                    Console.WriteLine(LocalizedStrings.Str2987);

                                    // запускаем экспорт стакана
                                    trader.RegisterMarketDepth(_sber);

                                    if (_portfolio != null && _depth != null)
                                        waitHandle.Set();
                                }
                            }
                        };

                        // подписываемся на событие появления моих новых сделок
                        trader.NewMyTrade += myTrade =>
                        {
                            var trade = myTrade.Trade;
                            Console.WriteLine(LocalizedStrings.Str2173Params, trade.Id, trade.Price, trade.Security.Code, trade.Volume, trade.Time);
                        };

                        // подписываемся на событие обновления стакана
                        trader.MarketDepthChanged += depth =>
                        {
                            if (_depth == null && _sber != null && depth.Security == _sber)
                            {
                                _depth = depth;

                                Console.WriteLine(LocalizedStrings.Str2988);

                                // если портфель и инструмент уже появился, то извещаем об этом основной поток для выставления заявки
                                if (_portfolio != null && _sber != null)
                                    waitHandle.Set();
                            }
                        };

                        Console.WriteLine(LocalizedStrings.Str2989Params.Put(account));

                        // дожидаемся появления портфеля и инструмента
                        waitHandle.WaitOne();


                        Sides _direction;

                        while (true)
                        {
                            if (GetBidsVolumes(VolumeDensity) > GetAsksVolumes(VolumeDensity)) // если объём бидов больше, чем объём асков
                            {
                                // MakeOrder(Sides.Buy);
                            }
                           /* else
                            {
                                _direction = Sides.Sell;
                                // MakeOrder(Sides.Sell);
                            }*/

                            var order = new Order
                            {
                                Type = OrderTypes.Market,
                                Portfolio = _portfolio,
                                // Price = _sber.ShrinkPrice(_lkoh.BestBid.Price + mid.Value),
                                Security = _sber,
                                Volume = 1,
                                Direction = Sides.Buy,
                            };
                            trader.RegisterOrder(order);
                            Console.WriteLine(LocalizedStrings.Str1157Params, order.Id);


                            /*var stoporder = new Order
                            {
                                Type = OrderTypes.Conditional,
                                Volume = 1,
                                Price = 900,
                                Security = _sber,
                                Direction = OrderDirections.Sell,
                                Condition = new QuikOrderCondition
                                {
                                    Type = QuikOrderConditionTypes.LinkedOrder,
                                    StopPrice = 900,
                                },
                            };*/
                            var stopOrder = new Order
                            {
                                Type = OrderTypes.Conditional,
                                Volume = 1,
                                Price = order.Price - 10,
                                Portfolio = _portfolio,
                                Security = _sber,
                                Direction = Sides.Sell,
                               // Comment = "Placed by TradingTrainer",
                               // ExpiryDate = DateTime.MaxValue,
                                Condition = new QuikOrderCondition
                                {
                                    Type = QuikOrderConditionTypes.LinkedOrder,
                                    LinkedOrderPrice = order.Price + 20,
                                    LinkedOrderCancel = false,
                                    StopPrice = order.Price - 10,
                                    ActiveTime = null,
                                },
                            };
                            trader.RegisterOrder(stopOrder);
                            Console.WriteLine(LocalizedStrings.Str1157Params, stopOrder.Id);
                            break;

                            // ждем 1 секунду
                            Thread.Sleep(1000);
                        }

                        // останавливаем подключение
                        trader.Disconnect();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadKey();
        }

        private static decimal GetAsksVolumes(int numberOfElements)
        {
            decimal sum = 0;

            Quote[] asks = _depth.Asks;//Connector.GetMarketDepth(Security).Asks;

            for (int i = 0; i < numberOfElements; i++)
            {
                sum += asks[i].Volume;
            }
            return sum;
        }
        private static decimal GetBidsVolumes(int numberOfElements)
        {
            decimal sum = 0;
            Quote[] bids = _depth.Bids; // Connector.GetMarketDepth(Security).Bids;

            for (int i = 0; i < numberOfElements; i++)
            {
                sum += bids[i].Volume;
            }
            return sum;
        }
    }
}