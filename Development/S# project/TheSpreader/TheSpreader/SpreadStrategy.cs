﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using StockSharp.Algo.Strategies;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Strategies.Quoting;
using StockSharp.Algo;
using StockSharp.BusinessEntities;
using StockSharp.Quik;
using StockSharp.Logging;
using StockSharp.Messages;

using Ecng.Collections;
using Ecng.Common;
using Ecng.Xaml;

namespace TheSpreader
{

    class SpreadStrategy : Strategy
    {
        public const int VolumeDensity = 5; // Количество асков и бидов, которые хотим анализировать для поиска суммы объёмов в аске или биде
        public decimal SignalSpreadSize = 1;

        protected override void OnStarted()
        {

           // Connector.RegisterMarketDepth(Security); // Регистрируем стакан
            //Trader.RegisterMarketDepth(Security);

            // Connector.MarketDepthChanged += depth => StrategyMainLogic();

            Connector.GetMarketDepth(Security).QuotesChanged += StrategyMainLogic;
            //Connector.GetMarketDepth(Security).QuotesChanged += StrategyMainLogic; // Событие обновление стакана
            base.OnStarted();
        }
        public void Run()
        {
            Connector.GetMarketDepth(Security).QuotesChanged += StrategyMainLogic;
            //Connector.MarketDepthChanged += depth => StrategyMainLogic();
        }

       private void StrategyMainLogic()
        {
            if (ProcessState == ProcessStates.Stopping)
            {
                return;
            }

            try
            {
                if (Position == 0)
                {
                    //if (SignalSpreadSize < Security.BestPair.SpreadPrice) // если указанный размер спреда меньше текущего
                    //{
                    //    if (GetActiveOrders().Count() != 0) // всегда снимаем активные заявки перед активизацией новых заявок
                    //    {
                    //        foreach (var order in GetActiveOrders())
                    //        {
                    //            Trader.CancelOrder(order);
                    //        }
                    //    }
                    //    return;
                    //}

                    if (GetActiveOrders().Count() != 0)
                    {

                        Connector.CancelOrders(null, null, null, null, Security);
                        //foreach (var order in GetActiveOrders())
                        //{
                        //    Trader.CancelOrder(order);
                        //}
                    }

                    if (GetBidsVolumes(VolumeDensity) > GetAsksVolumes(VolumeDensity)) // если объём бидов больше, чем объём асков
                    {
                        MakeOrder(Sides.Buy);
                    }
                    else
                    {
                        MakeOrder(Sides.Sell);
                    }
                }
                else
                {
                    if (GetActiveOrders().Count() != 0)
                    {
                        Connector.CancelOrders(null, null, null, null, Security);
                        //foreach (var order in GetActiveOrders())
                        //{
                        //    Trader.CancelOrder(order);
                        //}
                    }
                    MakeOrder(Position.GetDirection() == Sides.Buy ? Sides.Sell : Sides.Buy, Position);
                }
            }
            catch (Exception ex)
            {
                Task.Run(() =>
                    {
                        using (var fs = new StreamWriter("log.txt"))
                            fs.Write("Time: {0}, {1}", DateTime.Now, ex.Message);
                    });
            }
        }

        private decimal GetAsksVolumes(int numberOfElements)
        {
            decimal sum = 0;

            Quote[] asks = Connector.GetMarketDepth(Security).Asks;

            for (int i = 0; i < numberOfElements; i++)
            {
                sum += asks[i].Volume;
            }
            return sum;
        }
        private decimal GetBidsVolumes(int numberOfElements)
        {
            decimal sum = 0;
            Quote[] bids = Connector.GetMarketDepth(Security).Bids;

            for (int i = 0; i < numberOfElements; i++)
            {
                sum += bids[i].Volume;
            }
            return sum;
        }

        private IEnumerable<Order> GetActiveOrders()
        {
            var activeOrders = from order in Orders
                               where order.State == OrderStates.Active
                               select order;
            return activeOrders;
        }

        private void MakeOrder(Sides direction)
        {
            //if (ProcessState == ProcessStates.Stopping)
            //{
            //    return;
            //}

            var strategy = new MarketQuotingStrategy(direction, Volume);
            ChildStrategies.Add(strategy);
        }

        private void MakeOrder(Sides direction, decimal volume)
        {
            //if (ProcessState == ProcessStates.Stopping)
            //{
            //    return;
            //}

            var strategy = new MarketQuotingStrategy(direction, Math.Abs(volume));
            ChildStrategies.Add(strategy);
        }
    }
}