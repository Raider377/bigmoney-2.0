﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using StockSharp.Algo;
using StockSharp.BusinessEntities;
using StockSharp.Algo.Strategies.Reporting;
using StockSharp.Logging;
using StockSharp.Quik;
using StockSharp.Xaml;

using Ecng.Collections;
using Ecng.Common;
using Ecng.Xaml;

namespace TheSpreader
{
    public partial class MainWindow
    {
        public MyTradesWindow tradesWnd;
        private SpreadStrategy strategy;
        public QuikTrader Trader { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            tradesWnd = new MyTradesWindow();
        }

        private void ConnectBtn_Click(object sender, RoutedEventArgs e)
        {
            var quikPath = QuikTerminal.GetDefaultPath();
            Trader = new QuikTrader(quikPath);           

            Trader.Connect();

            MySecurities.SecurityProvider = Trader;            
            MyPortfolios.Portfolios = new PortfolioDataSource(Trader);          
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            if (strategy == null)
            {
                strategy = new SpreadStrategy
                {                  
                    Connector = Trader,
                    Security = MySecurities.SelectedSecurity,
                    Portfolio = MyPortfolios.SelectedPortfolio,
                    Volume = 1,
                    SignalSpreadSize = MySecurities.SelectedSecurity.PriceStep.Value
                };
            }

            Trader.NewOrders += orders => this.GuiAsync(() => tradesWnd.MyOrders.Orders.AddRange(orders));

            strategy.PropertyChanged += (send, args) => this.GuiAsync(() =>
                {
                    StateLbl.Content = strategy.ProcessState;
                    PnLLbl.Content = strategy.PnL;
                });


            Trader.RegisterMarketDepth(MySecurities.SelectedSecurity);
            strategy.Start();

            strategy.Run();
            //Trader.GetMarketDepth(MySecurities.SelectedSecurity).QuotesChanged += SpreadStrategy.OnStarted;
        }

        private void MyTradesBtn_Click(object sender, RoutedEventArgs e)
        {
            tradesWnd.Show();
        }
    }
}